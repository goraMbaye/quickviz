import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';


@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css']
})
export class BarchartComponent {

//   barChartOptions: ChartOptions = {
//      responsive: true,
//   };
 barChartLabels: Label[] = ['Apple', 'Banana', 'Kiwifruit', 'Blueberry', 'Orange', 'Grapes'];
//  barChartType: ChartType = 'bar';
//   barChartLegend = true;
//   barChartPlugins = [];

   barChartData2: ChartDataSets[] = [
    { data: [45, 36, 60,80,40]},
    { data: [20, 47, 60,15,51] },
    { data: [35, 67, 60,42,51] }
  ];
   public colors = [
    // {
    //   backgroundColor: 'rgba(250, 0, 0, 0.9)'
    // },
     {
       backgroundColor: 'rgba(255, 153, 0, 0.9)'
    },
     {
      backgroundColor: 'rgba(248, 242, 0, 0.9)'
    },
    {
      backgroundColor: 'rgba(0, 234, 0, 0.9)'
    },
    {
      backgroundColor: 'rgba(51, 153, 51, 0.9)'
    },
    ];
     
      public barChartOptions: any = {
        scales: {
      xAxes: [{
        stacked: true
      }],
      yAxes: [{
        stacked: true
      }]
    },
    maintainAspectRatio: false,
    legend: {
      display: true
    },
        scaleShowVerticalLines: false,
        responsive: true
      };
      public barChartLabels2: string[] = [];
     
      public barChartType = 'bar';
      public barChartLegend = true;


  // constructor() { }

  // ngOnInit() {
  // }

}
